package com.jesse.data.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringUtils {
	private static ApplicationContext context =null;
	static {
			context = (ApplicationContext) new ClassPathXmlApplicationContext(new String [] {"/applicationContext.xml"}) ;
	}
	private SpringUtils() {
		
	}
	public static Object getBean(String name){
		return context.getBean(name);
	}
}
