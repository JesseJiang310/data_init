package com.jesse.data.util;

import java.io.OutputStream;
import java.io.PrintStream;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;

public class LogPrint extends PrintStream {
	private Text text;
	 
	public LogPrint(OutputStream out, Text text) {
		super(out);
		this.text = text;
	}
	public void write(byte[] buf, int off, int len) {
		final String message = new String(buf, off, len);
 
		Display.getDefault().syncExec(new Thread() {
			public void run() {
				if (text != null && !text.isDisposed()) {
					if(text.getLineCount()>200) {
						text.setText("");
					}
					text.append(message);
				}
			}
		});
	}
}
