package com.jesse.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jesse.branch.pojo.Branch;
import com.jesse.data.util.SpringUtils;
import com.jesse.product.pojo.Product;
import com.jesse.product.service.ProductService;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;

class RsyncProductCallable implements Callable<Map<String, String>> {
	final Branch branch;
	private ProductService productService = (ProductService) SpringUtils.getBean("productServiceImpl");
	private Logger logger = LogManager.getLogger(getClass());
	public RsyncProductCallable(Branch branch) {
		this.branch = branch;
	}

	@Override
	public Map<String, String> call() throws Exception {
		Map<String, String> result = new HashMap<>();
		List<Product> list = new ArrayList<Product>();
		Jedis jedis = new Jedis(branch.getIp(), branch.getPort(), branch.getTimeout());
		try {
			list = productService.queryByDownloadAndBranch(false, branch, 1, 50);
			if (list.size() > 0) {
				String zedisResult = jedis.set("product:push", JSONObject.toJSONString(list));
				List<Integer> ids = JSONArray.parseArray(zedisResult, Integer.class);
				if (ids.size() > 0) {
					if (productService.updateDownload(true, ids, branch.getId())) {
						result.put("status", "OK");
						result.put("message", String.format("rsync product size: %d \n", ids.size()));
						result.put("finish", ids.size() < 50 ? "1" : "0");
					}
				}
			}else {
				result.put("status", "OK");
				result.put("finish", "1");
			}
		} catch (JedisConnectionException e) {
			result.put("status", "ERROR");
			result.put("message", "Lost connection with branch\n");
		} catch (Exception e) {
			logger.error(e);
			result.put("status", "ERROR");
			result.put("message", "SYSERROR\n");
		} finally {
			jedis.disconnect();
		}
		return result;
	}
}