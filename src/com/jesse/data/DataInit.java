package com.jesse.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jesse.branch.pojo.Branch;
import com.jesse.branch.service.BranchService;
import com.jesse.data.util.LogPrint;
import com.jesse.data.util.SpringUtils;
import com.jesse.product.service.ProductService;

public class DataInit {

	protected Shell shell;
	private BranchService branchService = (BranchService) SpringUtils.getBean("branchServiceImpl");
	private ProductService productService = (ProductService) SpringUtils.getBean("productServiceImpl");
	private Text text;
	private TableViewer tableViewer;
	private Logger logger = LoggerFactory.getLogger(getClass());
	private ExecutorService executor = Executors.newSingleThreadExecutor();
	private Composite composite;
	private Button btnDataInit;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			DataInit window = new DataInit();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		System.exit(0);
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(1024, 768);
		shell.setText("SWT Application");
		shell.setLayout(new GridLayout(2, false));
		
		Button btnLoadBranch = new Button(shell, SWT.NONE);

		btnLoadBranch.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
		btnLoadBranch.setText("Fresh Branch");

		composite = new Composite(shell, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		composite.setLayout(new RowLayout(SWT.HORIZONTAL));

		btnDataInit = new Button(composite, SWT.NONE);
		btnDataInit.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
		btnDataInit.setText("Data Init");

		btnDataInit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Branch branch = (Branch) tableViewer.getStructuredSelection().getFirstElement();
				if (branch != null) {
					if (MessageDialog.openConfirm(shell, "Are you sure???",
							String.format("Are you sure to init product data for branch %s", branch.getName()))) {
						int result = productService.initBranchProduct(branch.getId());
						showLog(branch.getName(),String.format("init data size:%d \n", result));
					}
					if (MessageDialog.openConfirm(shell, "Are you sure???",
							String.format("Are you sure to transfer product data to branch %s", branch.getName()))) {
						
						rsyncProduct(branch, executor);
					}
				} else {
					MessageDialog.openError(shell, "Error", "Please choose one branch from the branch list");
				}
			}

		});
		tableViewer = new TableViewer(shell, SWT.BORDER | SWT.FULL_SELECTION);
		Table table = tableViewer.getTable();
		table.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
		GridData gd_table = new GridData(SWT.LEFT, SWT.FILL, false, true, 1, 1);
		gd_table.widthHint = 510;
		table.setLayoutData(gd_table);

		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		TableColumn column = new TableColumn(table, SWT.NULL);
		column.setText("Id");
		column.setWidth(40);
		TableViewerColumn viewerColumn = new TableViewerColumn(tableViewer, column);
		viewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Branch branch = (Branch) element;
				return branch.getId().toString();
			}
		});

		column = new TableColumn(table, SWT.NULL);
		column.setText("Branch Name");
		column.setWidth(160);
		viewerColumn = new TableViewerColumn(tableViewer, column);
		viewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Branch branch = (Branch) element;
				return branch.getName();
			}
		});
		column = new TableColumn(table, SWT.NULL);
		column.setText("IP");
		column.setWidth(160);
		viewerColumn = new TableViewerColumn(tableViewer, column);
		viewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Branch branch = (Branch) element;
				return branch.getIp();
			}
		});

		text = new Text(shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL | SWT.MULTI);
		text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tableViewer.setContentProvider(new IStructuredContentProvider() {
			@Override
			public Object[] getElements(Object arg0) {
				return ((List<Branch>) arg0).toArray();
			}
		});
		tableViewer.setInput(branchService.getAllBranch());
		btnLoadBranch.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				tableViewer.setInput(branchService.getAllBranch());
			}
		});
		LogPrint logPrint = new LogPrint(System.out, text);
		System.setOut(logPrint);
		System.setErr(logPrint);
	}

	private void rsyncProduct(Branch branch, ExecutorService executor) {
		Future<Map<String, String>> future = executor.submit(new RsyncProductCallable(branch));
		try {
			Map<String, String> result = future.get();
			Display.getCurrent().asyncExec(new Runnable() {

				@Override
				public void run() {
					if (result != null && !StringUtils.isEmpty(result.get("message"))) {
						showLog(branch.getName(), result.get("message"));
					}
					if ("1".equalsIgnoreCase(result.get("finish"))) {
						showLog(branch.getName(), "finish init data ");
						return;
					}
				}
			});

		} catch (Exception e1) {
			e1.printStackTrace();
			logger.error("rsync product error", e1);
			showLog(branch.getName(), "Error");
			return;
		}
		Display.getCurrent().timerExec(5000, new Runnable() {

			@Override
			public void run() {
				rsyncProduct(branch, executor);
			}
		});

	}

	private void showLog(String branch, String content) {
		System.out.println(String.format("%s %s %s", new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime()), branch, content));

	};
}
